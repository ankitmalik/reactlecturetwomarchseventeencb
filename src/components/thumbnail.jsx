import React from 'react';

export default class Thumbnail extends React.Component{
    onButtonClick(e){
console.log("Hello World");
        this.props.setCurrent(this.props.title, this.props.url);

    }
    render(){
        return (
            <span style={ {'marginLeft' : '20px', 'marginRight' : '20px'} }>
            <button className="btn btn-default btn-lg" onClick={this.onButtonClick.bind(this)}>
                {this.props.title}
            </button>
            </span>
        );
    }

}


