


// import React and ReactDOM
import * as React from "react";
import * as ReactDOM from "react-dom";

// Create a React Component for our hello world
class HelloComponent extends React.Component{
    render() {
        return <div> Hello World from React! </div>
    }
}





// Create an object of this class
var my_hello_world_object = <HelloComponent/>;
var node = document.getElementById("app");

ReactDOM.render(my_hello_world_object, node);


