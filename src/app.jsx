import ReactDOM from 'react-dom';
import React from 'react';

import MainBox from './components/main-box.jsx';
import Thumbnail from './components/thumbnail.jsx';
import data from './data';


class App extends React.Component{

    constructor(props){
        super(props);
        this.state = {current_title: data[0].title, current_url: data[0].url};
        this.setCurrent = this.setCurrent.bind(this);

    }

    setCurrent(title, url){
        console.log("title, url")
        this.setState( {current_title:title, current_url:url} )
    }

    render(){
        return (
        <div>
            <div className="row jumbotron">
                <div className="col-md-4"></div>
                <div className="col-md-4">

                    <MainBox title={this.state.current_title} url={this.state.current_url}/>
                </div>
                <div className="col-md-4"></div>

            </div>

            <div className="row">
                <div className="col-md-4"></div>
                <div className="col-md-4">

            { data.map(
                function(object){return (
                    <Thumbnail key={object.title} title={object.title} url={object.url} setCurrent={this.setCurrent}/>
                )}.bind(this))}


                </div>
                <div className="col-md-4"></div>


            </div>

        </div>
        );
    }
}

ReactDOM.render(<App/>, document.getElementById("app"));